module.exports = function (grunt) {
  // Configure grunt
  grunt.initConfig({

    uglify: {

      application: {
        options: {
          sourceMap: true
        },
        files: { 'assets/js/main.js' : [ '_assets/js/*.js' ] }
      },

      plugins: {
        options: {
          sourceMap: true
        },
        files: { 'assets/js/plugins.js' : [ '_assets/js/plugins/*.js' ] }
      }
    },

    compass: {

      build: {
        options: {
          config: 'config.rb',
          sourcemap: true
        }
      }

    }, // compass

    imagemin: {

      build: {
        options: {
          optimizationLevel: 7
        },
        files: [{
          expand: true,
          cwd: '_assets/img/',
          src: ['**/*.{png,jpg,gif}', '!icon/*.{png,jpg,gif}'],
          dest: 'assets/img/'
        }]
      } // build

    }, // imagemin
  notify: {
    js: {
      options: {
        title: 'JS',
        message: 'JS is ok!'
      }
    },
    css: {
      options: {
        title: 'CSS',
        message: 'CSS is ok!'
      }
    },
    img: {
      options: {
        title: 'IMG',
        message: 'IMG is ok!'
      }
    },
    watch: {
      options: {
        title: 'Watch',
        message: 'Grunt is watching files (:'
      }
    }
  },
    watch: {
      options: {
        spawn: false,
        livereload: true
      },
      css: {
        files: [ '_assets/sass/**/*.scss', '_assets/img/icon/*.{png,jpg,gif}' ],
        tasks: [ 'compass' ]
      },
      img: {
        files: [ '!_assets/img/icon/', '_assets/img/**/*.{png,jpg,gif}' ],
        tasks: [ 'imagemin' ]
      },
      js: {
        files: [ '_assets/js/**/*.js' ],
        tasks: [ 'uglify' ]
      }
    }

  });

  // Load all plugins
  require('load-grunt-tasks')(grunt);

  //Tasks
  grunt.registerTask('default', ['uglify', 'sprite', 'stylus', 'imagemin']);

  //Watch task
  grunt.registerTask('w', ['watch']);
}
